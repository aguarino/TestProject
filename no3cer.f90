module mod_NO3CER
	
	use mod_Globals
	use mod_Errors
	use mod_Ppkg
	use, intrinsic :: ieee_exceptions
	
		
	implicit none
	public :: no3CER

	real(8), parameter          :: Mw_water = 18.03d-3
	real(8), parameter          :: Mw_acid  = 63d-3
	
	contains    
	

	
subroutine no3CER(ppkgID, nc, Pg_in, Tg_in, ng_in, Pc_in, Tc_in, nc_in, Nsteps)
	
		integer, intent(in)             :: ppkgID           ! index of the property package
		integer, intent(in)             :: nc               ! number of components in the stream
		real(8), intent(in)             :: Pg_in            ! inlet pressure [Pa]           process side
		real(8), intent(in)             :: Tg_in            ! inlet temperature [K]         process side
		real(8), intent(in)             :: ng_in(nc)        ! inlet molar flows [mol/s]     process side
		real(8), intent(in)             :: Pc_in            ! inlet pressure [Pa]           coolant side
		real(8), intent(in)             :: Tc_in            ! inlet temperature [K]         coolant side
		real(8), intent(in)             :: nc_in(nc)        ! inlet molar flows [mol/s]     coolant side
		
		integer, intent(in)             :: Nsteps           ! number of steps for the iterations   
	
		real(8)                     :: Tg(0:Nsteps),Tc(0:Nsteps)        ! temperature arrays      
		real(8)                     :: Qg(0:Nsteps),Qc(0:Nsteps)        ! heat flow arrays
		real(8)                     :: hQ(Nsteps)                       ! heat exchange coefficients
		real(8)                     :: TI(NSteps)                       ! interface temperature
		real(8)                     :: dQ(Nsteps)                       ! exchanged heat
		real(8)                     :: dV(NSteps)                       ! volume size
		real(8)                     :: hQ_c(Nsteps), hQ_g(Nsteps), UA
		
		real(8)                     :: SL, VL                                               ! exchange surface per unit lenght, tube volume per unit lenght
		real(8)                     :: Water_St, Tube_St
		real(8)                     :: mcg, mcc, beta
		integer                     :: i, j, k, res
		real(8)                     :: yg(nc), yc(nc), nL(nc), nG(nc), nG_loc(nc), nL_loc(nc)
		real(8)                     ::  nc0, ng0, mc, mg                ! initial molar flows, mass flows
		real(8)                     :: Qguess, EnthalpyError, Qtot
		real(8)                     :: jQcond
		real(8)                     :: Tg_loc, Tc_loc, tau
		real(8)                     :: Vtot, Vdes
		real(8)                     :: HH, CC
		
		real(8)             :: de               ! external tube diameter [m]
		real(8)             :: di               ! internal tube diameter [m]
		real(8)             :: L, dL            ! tube length [m], tube length per step
		integer             :: Ntubes           ! number of tubes
		real(8)                     :: fouling
		real(8)                     :: pitch, baffle_de, baffle_di, baffle_L
		real(8)                     :: eff
		
		integer, parameter          :: MAX_ITER = 20
		real(8), parameter          :: alfa = 0.7d0
		
		baffle_de = 1.72d0
		baffle_di = 0.7d0
		baffle_L  = 0.1d0
		pitch     = 0.024d0
		de        = 0.01905d0
		di        = 0.01575d0
		L         = 4.35d0
		Ntubes    = 3469
		fouling   = 4.1d-4*4.184d0/3.6d0
		eff       = 0.18d0
		
		dL        = L / Nsteps
		
		SL   = PI * de * Ntubes
		VL   = 0.25d0 * PI * di**2 * Ntubes
		Vtot = VL*L
		
		Water_St = PI * 0.5d0*(baffle_de+baffle_di) * baffle_L * ((pitch-de)/(pitch*sqrt(3d0)/2d0))
		Tube_St  = PI * 0.25d0 * di**2 * Ntubes
		
		open (unit = 2, file = "cer.txt", RECL=512)
		
		!=== INITIALIZATION ==========================
		!during initialization the exchanger is assumed to have no condensation and no reaction
		
		ng0=SMALL
		nc0=SMALL
		do i=1, nc
			ng0 = ng0 + ng_in(i)
			nc0 = nc0 + nc_in(i)
		end do
		
		do i=1, nc
			yc(i)=nc_in(i)/nc0
			yg(i)=ng_in(i)/ng0
		end do
		
		!calculates the mass flow in each stream (constant)
		mc=nc0*pkg_LiqProp(ppkgID,1,nc,Pc_in,Tc_in,yc,MOLAR_WEIGHT)
		mg=ng0*pkg_GasProp(ppkgID,1,nc,Pg_in,Tg_in,yg,MOLAR_WEIGHT)
				
		
		!first estimation of the heat exchange coefficients (it will likely be an underestimation)
		!todo: hide in subroutine?
		mcg=mg*pkg_GasProp(ppkgID,1,nc,Pg_in,Tg_in,yg,MASS_CP)
		mcc=mc*pkg_LiqProp(ppkgID,1,nc,Pc_in,Tc_in,yc,MASS_CP)
		
		hq_c(1)=heatExchangeCoeff(ppkgID, nc, Pc_in, Tc_in, nc_in, nc_in, Water_St, de, 1)
		hq_g(1)=heatExchangeCoeff(ppkgID, nc, Pg_in, Tg_in, ng_in, ng_in, Tube_St , di, 2)*(di/de)
		UA = (Ntubes*de*PI*L)/(1d0/hq_c(1)+1d0/hq_g(1)+fouling)
		Qguess = (Tg_in-Tc_in)*(1d0-exp(-UA*(1d0/mcc-1d0/mcg)))/(1d0/mcc-exp(-UA*(1d0/mcc-1d0/mcg))/mcg)
		
		!once we have the Qguess we can initialize all the streams 
		Tg(0)=Tg_in
		Qg(0)=pkg_GasProp(ppkgID,1,nc,Pg_in,Tg(0),yg,MOLAR_ENTHALPY)*ng0
		
		Tc(Nsteps) = Tc_in
		Qc(NSteps) = pkg_LiqProp(ppkgID,1,nc,Pc_in,Tc(Nsteps),yc,MOLAR_ENTHALPY)*nc0
		
		do i=1, Nsteps
		
			dQ(i)=Qguess/Nsteps
			
			! initialization coolant side
			Qc(NSteps-i) = Qc(NSteps-i+1)+dQ(i)
			res=pkg_PHFlash(ppkgID, 1, nc, Pc_in, Qc(NSteps-i)/nc0, nc_in, nL, nG, Tc(NSteps-i), beta)
			
			! initialization process side
			Qg(i) = Qg(i-1)-dQ(i)
			res=pkg_PHFlash(ppkgID, 1, nc, Pg_in, Qg(i)/ng0, ng_in, nL, nG, Tg(i), beta)
			
		end do
	
		EnthalpyError = (Qc(NSteps)+Qg(0))-(Qc(0)+Qg(NSteps))
		
		
		
		!===================================================================
		!
		! SOLVE LOOP
		!
		
		do k=1, MAX_ITER
		
			!================ PROCESS SIDE ==================
			Qtot  = 0d0
			Vdes = 0d0
			do j=1, nc
				nG_loc(j)=ng_in(j)
				nL_loc(j)=0d0
			end do
			
			do i=1, NSteps
				
				Tg_loc = 0.5d0*(Tg(i)+Tg(i-1))
				Tc_loc = 0.5d0*(Tc(i)+Tc(i-1))
				
				!---- reaction -----------
				
				call React(ppkgID, nc, Pg_in, Tg(i-1), Tg(i-1), nL_loc, nG_loc, VL*dL, eff, HH, CC)
				
				!-------------------------
								
				!calculate the new heat exchange coefficients
				hq_g(i)=heatExchangeCoeff(ppkgID, nc, Pg_in, Tg_loc, nL_loc, nG_loc, Tube_St , di, 2)*(di/de)
				hq_c(i)=heatExchangeCoeff(ppkgID, nc, Pc_in, Tc_loc, nc_in, nc_in, Water_St, de, 1)
				hq_c(i)= 1d0/(1d0/hq_c(i)+fouling)                            ! fouling applied to water only
				
				if(nL_loc(7)<1d-10) then
					!no condensation
					Vdes = Vdes + VL*dL
					
					TI(i) = (hq_g(i)*Tg_loc+hq_c(i)*Tc_loc)/(hq_g(i)+hq_c(i))     ! estimate the interface temperature
				else
					!there's condensation, we calculate the interface temperature in a special way
					call  InterfaceBalance(ppkgID, nc, Pg_in, Tg_loc, Tc_loc, nG_loc, nL_loc, hq_g(i), hq_c(i), jQcond, TI(i))
					
					TI(i) = (hq_g(i)*Tg_loc+hq_c(i)*Tc_loc)/(hq_g(i)+hq_c(i))   
					!apparent hg, actually not used
					hq_g(i) = hq_c(i)*(TI(i)-Tc_loc)/(Tg_loc-TI(i))
				end if
				
				tau  = TI(i)/Tg_loc
				
				Tg(i)=(CC*Tg(i-1)+hq_c(i)*SL*dL*Tc_loc+(Qg(i-1)-HH))/(CC+hq_c(i)*SL*dL*tau)
				
				!update the enthalpy and temperature on the process side
				dQ(i) = hq_c(i)*SL*dL*(TI(i)-Tc_loc)*alfa+(1d0-alfa)*dQ(i)
				Qg(i) = Qg(i-1)-dQ(i)
				
				!calculate the cumulative duty
				Qtot = Qtot + dQ(i)
				
			
			end do ! position loop
			
			!============ COOLANT SIDE =====================
			Qtot = 0d0
			do i=Nsteps, 1, -1
				Tg_loc = 0.5d0*(Tg(i)+Tg(i-1))
				Tc_loc = 0.5d0*(Tc(i)+Tc(i-1))
				
				!calculate the new heat exchange coefficients
				!hq_g(i)=heatExchangeCoeff(ppkgID, nc, Pg_in, Tg_loc, ng_in, ng_in, Tube_St , di, 2)*(di/de)
				hq_c(i)=heatExchangeCoeff(ppkgID, nc, Pc_in, Tc_loc, nc_in, nc_in, Water_St, de, 1)
				hq_c(i)= 1d0/(1d0/hq_c(i)+fouling)                            ! fouling applied to water only
				hQ(i) = 1d0/(1d0/hq_c(i)+1d0/hq_g(i))
				
				TI(i) = (hq_g(i)*Tg_loc+hq_c(i)*Tc_loc)/(hq_g(i)+hq_c(i))     ! estimate the interface temperature
				dQ(i) = hq_c(i)*SL*dL*(TI(i)-Tc_loc)*alfa+(1d0-alfa)*dQ(i)
				
				!update the enthalpy and temperature on the coolant side
				Qc(i-1) = Qc(i)+dQ(i)
				res=pkg_PHFlash(ppkgID, 1, nc, Pc_in, Qc(i-1)/nc0, nc_in, nL, nG, Tc(i-1), beta)
				
				!calculate the cumulative duty
				Qtot = Qtot + dQ(i)
				
			
			end do ! position loop
			
			EnthalpyError = (Qc(NSteps)+Qg(0))-(Qc(0)+Qg(NSteps))
			
			do i=0, Nsteps
				if(i.eq.0) then
					write (2,*) i, char(9), Tg(i), char(9), 0d0, char(9), Tc(i)
				else
					write (2,*) i, char(9), Tg(i), char(9), TI(i), char(9), Tc(i)
				end if
			end do
			
			write(2,*) "-----------------"
			
		end do  ! solve loop
		
		close(2)
		
	
end subroutine no3CER

	subroutine React(ppkgID, nc, P, Tin, Tout, nL, nG, Volume, Efficiency, HH, CC)
		integer, intent(in)         :: ppkgID
		integer, intent(in)         :: nc
		real(8), intent(in)         :: P
		real(8), intent(in)         :: Tin
		real(8), intent(in)         :: Tout
		real(8), intent(inout)      :: nL(nc)
		real(8), intent(inout)      :: nG(nc)
		real(8), intent(in)         :: Volume           ! for oxydation
		real(8), intent(in)         :: Efficiency       ! for condensation
		real(8), intent(inout)      :: HH               ! OutletHeatFlow = HH + CC*(T-Tout)
		real(8), intent(inout)      :: CC
		
		real(8), parameter          :: dT=1d0
		real(8)                     :: nLin(nc), nGin(nc), y(nc), x(nc)
		real(8)                     :: nL0, nG0
		real(8)                     :: HH1
		integer                     :: i
		!keep a copy of the inlet conditions
		
		do i=1, nc
			nLin(i)=nL(i)
			nGin(i)=nG(i)
		end do
		
		call no3_Oxydize(nc, P, Tin, Tout, nL, nG, Volume)
		call no3_Condensation(nc,P,Tout,nL,nG,Efficiency)
	
		nL0 = sum(nL)+SMALL
		nG0 = sum(nG)+SMALL
		y = nG/nG0
		x = nL/nL0
		
		! outlet enthalpy flow
		HH = pkg_GasProp(ppkgID,1,nc,P,Tout,y,MOLAR_ENTHALPY)*nG0+pkg_LiqProp(ppkgID,1,nc,P,Tout,x,MOLAR_ENTHALPY)*nL0
		
		! derivative is calculated using dT
		call no3_Oxydize(nc, P, Tin, Tout+dT, nLin, nGin, Volume)
		call no3_Condensation(nc,P,Tout+dT,nLin,nGin,Efficiency)
	
		nL0 = sum(nLin)+SMALL
		nG0 = sum(nGin)+SMALL
		y = nGin/nG0
		x = nLin/nL0
		
		! outlet enthalpy flow
		HH1 = pkg_GasProp(ppkgID,1,nc,P,Tout+dT,y,MOLAR_ENTHALPY)*nG0+pkg_LiqProp(ppkgID,1,nc,P,Tout+dT,x,MOLAR_ENTHALPY)*nL0
		CC = (HH1-HH)/dT
		
	end subroutine React


	real(8) function heatExchangeCoeff(ppkgID, nc, P, T, nL, nG, flowarea, d, htype)
		integer, intent(in)         :: ppkgID
		integer, intent(in)         :: nc
		real(8), intent(in)         :: P
		real(8), intent(in)         :: T
		real(8), intent(in)         :: nL(nc)
		real(8), intent(in)         :: nG(nc)
		real(8), intent(in)         :: flowarea     ! the cross section to obtain rho_v from mflow
		real(8), intent(in)         :: d            ! characteristic lenght 
		integer, intent(in)         :: htype
		
		real(8)                     :: x(nc), y(nc), nG0, nL0
		real(8)                     :: kT, mu, cp, rho_v, Re, Pr, Mw
		integer                     :: i
		
		nG0=0d0+SMALL
		nL0=0d0+SMALL
		do i=1, nc
			nG0=nG(i)+nG0
			nL0=nL(i)+nL0
		end do
		
		do i=1, nc
			x(i)=nL(i)/nL0
			y(i)=nG(i)/nG0
		end do
		
		select case(htype)
		case(1)  !shell type
			Mw    = pkg_LiqProp(ppkgID,1,nc,P,T,x,MOLAR_WEIGHT)
			kT    = pkg_LiqProp(ppkgID,1,nc,P,T,x,THERMAL_CONDUCTIVITY)
			mu    = pkg_LiqProp(ppkgID,1,nc,P,T,x,VISCOSITY)
			cp    = pkg_LiqProp(ppkgID,1,nc,P,T,x,MASS_CP)
			rho_v = nL0 * Mw / flowarea                                            !kg/s m2
			Re    = rho_v*d/mu
			Pr    = cp*mu/kT
		
			heatExchangeCoeff =  kT/d*0.25d0*(Re**0.6d0)*(Pr**0.33d0) 
			
		case(2) !tube type (gas only)
			Mw    = pkg_GasProp(ppkgID,1,nc,P,T,y,MOLAR_WEIGHT)
			kT    = pkg_GasProp(ppkgID,1,nc,P,T,y,THERMAL_CONDUCTIVITY)
			mu    = pkg_GasProp(ppkgID,1,nc,P,T,y,VISCOSITY)
			cp    = pkg_GasProp(ppkgID,1,nc,P,T,y,MASS_CP)
			rho_v = nG0 * Mw / flowarea                                            !kg/s m2
			Re    = rho_v*d/mu
			Pr    = cp*mu/kT
			
			heatExchangeCoeff = kT/d* 0.023d0*(Re**0.8)*(Pr**0.33d0)
			
		end select
	end function heatExchangeCoeff

	! calculate the heat flux due to mass diffusion towards the condensing film
	subroutine InterfaceBalance(ppkgID, nc, P, Tg, Tw, nG, nL, hG, hW, jQcond, TI)
		integer, intent(in)         :: ppkgID
		integer, intent(in)         :: nc
		real(8), intent(in)         :: P            ! gas stream pressure
		real(8), intent(in)         :: Tg           ! gas stream temperature
		real(8), intent(in)         :: Tw           ! coolant temperature
		real(8), intent(in)         :: nG(nc)       ! gas stream composition
		real(8), intent(in)         :: nL(nc)       ! gas stream composition
		real(8), intent(in)         :: hG           ! gas stream naked heat transfer coefficient
		real(8), intent(in)         :: hW           ! water fouled heat trasnfer coefficient
		real(8), intent(inout)      :: jQcond       ! increased heat flow due to condensation
		real(8), intent(inout)      :: TI           ! interface temperature at the condesning film
		
		real(8)                     :: Pr, Sc, Re
		real(8)                     :: PvapG, PvapI ! vapour pressure in gas and at interface
		real(8)                     :: Tr, A
		real(8)                     :: D            ! diffusion coefficient of the gas-water binary system
		real(8)                     :: Mw, kT, mu, cp, rho
		
		real(8)                     :: y(nc), nG0
		real(8)                     :: hm, hh
		real(8)                     :: Tguess, TIold
		real(8)                     :: w            ! acid fraction
		integer                     :: i, k
		integer, parameter          :: MAX_ITER = 20
		real(8)                     :: Ti1, Ti2, Tim    ! bisection parameters for interface temperature
		real(8)                     :: dT1, dT2, dTm
		
		
		
		nG0=0d0+SMALL
		do i=1, nc
			nG0=nG(i)+nG0
		end do
		
		do i=1, nc
			y(i)=nG(i)/nG0
		end do
		
		!acid fraction
		w = 1d0/(1d0+nL(nc-1)*Mw_water/(SMALL+nL(nc)*Mw_acid))
		
		
		Mw    = pkg_GasProp(ppkgID,1,nc,P,Tg,y,MOLAR_WEIGHT)
		kT    = pkg_GasProp(ppkgID,1,nc,P,Tg,y,THERMAL_CONDUCTIVITY)
		mu    = pkg_GasProp(ppkgID,1,nc,P,Tg,y,VISCOSITY)
		cp    = pkg_GasProp(ppkgID,1,nc,P,Tg,y,MASS_CP)
		rho   = pkg_GasProp(ppkgID,1,nc,P,Tg,y,MASS_DENSITY)
	   
		Pr    = cp*mu/kT
		D     = 6.667d-6               ! TODO, calculate
		Sc    = mu/(rho*D)             ! schmidt number
		
		hm    = hG / (cp*Mw) * (Pr/Sc)**(0.6667d0)
		hh    = hm / (hw+hG)
		
		!calculation of the vapour pressure in gas (correlation)
		PvapG = Pvap(Tg,0d0)
		
		!estimation of the interface temperature by solving the energy balance at the interface
		Tguess = (hG*Tg+hw*Tw)/(hG+hw)
		
		!before starting the bisection we try to find the boundaries
		!f_auxInt is assumed to be monothonically decreasing
		Tim = Tguess
		dTm = f_auxInt(Tim, Tguess, P, w, PvapG, hh )
		
		if(dTm.gt.0d0) then
			Ti1 = Tim
			dT1 = dTm
			Ti2 = Tg
			dT2 = f_auxInt(Ti2, Tguess, P, w, PvapG, hh )
			do while (dT2.gt.0d0)
				Ti2 = Ti2 + 5d0
				dT2 = f_auxInt(Ti2, Tguess, P, w, PvapG, hh )
			end do
		else
			Ti2 = Tguess
			dT2 = dTm
			Ti1 = Tw
			dT1 = f_auxInt(Ti1,  Tguess,P, w, PvapG, hh )
			do while (dT1.lt.0d0)
				Ti1 = Ti1 - 5d0
				dT1 = f_auxInt(Ti1, Tguess, P, w, PvapG, hh )
			end do
		end if
		
		k=0
		do while ((abs(Ti2-Ti1).gt.1d-2).and.(k.lt.MAX_ITER))
			Tim = 0.5d0*(Ti2+Ti1)
			dTm = f_auxInt(Tim, Tguess, P, w, PvapG, hh )
			
			if(dTm*dT1.lt.0d0) then
				Ti2=Tim
				dT2=dTm
			else
				Ti1=Tim
				dT1=dTm
			end if
			k=k+1
		end do
		
		TI = Tim
		jQcond = hm*log(max(P-Pvap(Ti,w),SMALL)/(P-PvapG))*hVap(Ti)
		
	end subroutine InterfaceBalance

	!returns the approximated vapour pressure of water at a given temperature and acid fraction (Correlation of CER.exe)
	real(8) pure function Pvap(T,w)
		real(8), intent(in)     :: T
		real(8), intent(in)     :: w
		real(8)                 :: Tr, A
		Tr = 647.27-T
		A = (3.2437814d0 + ((5.86826d0 * Tr) / 1d3) + ((1.1702379d0 * Tr**3) / 1d8)) / (1d0 + ((2.1878462d0 * Tr) / 1d3))
		Pvap = 221.15d0 * 10**(-Tr*A/T) * 1d5
		
		Pvap = Pvap*(1d0-(w**1.4d0)/0.8d0-0.1d0*w)  
	end function
	
	!returns the condensation heat for water at given temperature (J/mol) (Correlation of CER.exe)
	real(8) pure function hVap(T)
		real(8), intent(in)     :: T
		real(8)                 :: T_C
		T_C = max(0d0,T-C_TO_K)
		hVap = (597.2d0-(T_C**1.012/1.84d0))*4184d0         ! converted from kcal/kg to J/kg
		hVap = hVap*Mw_water                                ! converted to J/mol
	end function
	
	!used in the bisection method to find the interface temperature
	real(8) pure function f_auxInt(T,Tguess,P,w,PvapG,hh)
		real(8), intent(in)     :: T
		real(8), intent(in)     :: Tguess
		real(8), intent(in)     :: P
		real(8), intent(in)     :: w
		real(8), intent(in)     :: PvapG        ! water vapour pressure in the bulk gas
		real(8), intent(in)     :: hh           ! hm/(hg+hw)
		real(8)                 :: PvapI        ! water vapour pressure at the interface 
	
		PvapI = Pvap(T,w)
		f_auxInt = Tguess+hh*log(max(P-PvapI,SMALL)/(P-PvapG))*hVap(T)-T
	end function f_auxInt
	
end module